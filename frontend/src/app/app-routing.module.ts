import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'mesa-casino',
    loadChildren: () => import('./casino/casino.module').then( m => m.CasinoModule )
  },
  {
    path: '**',
    // component: ErrorPageComponent,
    redirectTo: 'mesa-casino'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
