export interface HistoricoApuestas {
  ok:                boolean;
  msg:               string;
  historicoApuestas: HistoricoApuesta[];
}

export interface HistoricoApuesta {
  fecha:     Date;
  ganadores: any[];
  _id:       string;
  numero_ganador: number;
  apuestas:  Apuesta[];
  __v:       number;
}

export interface Apuesta {
  jugador:      Jugador;
  color:        string;
  numero:       number;
  apuesta:      number;
  valorApuesta: string;
}

export interface Jugador {
  dinero:         number;
  estado:         boolean;
  _id:            string;
  nombre:         string;
  identificacion: string;
  __v:            number;
}
