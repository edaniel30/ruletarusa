export interface NuevaApuesta {
  ok:           boolean;
  msg:          string;
  nuevaApuesta: NuevaApuestaClass;
}

export interface NuevaApuestaClass {
  fecha:     Date;
  ganadores: any[];
  _id:       string;
  apuestas:  Apuesta[];
  __v:       number;
}

export interface Apuesta {
  jugador:      Jugador;
  color:        string;
  numero:       number;
  apuesta:      number;
  valorApuesta: number;
}

export interface Jugador {
  nombre:         string;
  dinero:         number;
  estado:         boolean;
  identificacion: string;
  _id:            string;
}
