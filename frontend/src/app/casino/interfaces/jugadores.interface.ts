export interface Jugadores {
  ok:        boolean;
  msg:       string;
  jugadores: Jugador[];
}

export interface Jugador {
  dinero?:         number;
  estado?:         boolean;
  _id:             string;
  nombre?:         string;
  identificacion?: string;
  __v?:            number;
}
