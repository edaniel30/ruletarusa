import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'columName'
})
export class ColumNamePipe implements PipeTransform {

  transform( title: string ): string {

    const newTitle = title.replace( '_', ' ' );
    return newTitle;
  }

}

