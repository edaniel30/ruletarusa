import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HistoricoApuesta } from '../../interfaces/apuestas.interface';

@Component({
  selector: 'app-ver-apuestas',
  templateUrl: './ver-apuestas.component.html',
  styles: [
  ]
})
export class VerApuestasComponent implements OnInit {

  apuestas: any = [];

  constructor(  private dialogRef: MatDialogRef<VerApuestasComponent>,
                @Inject(MAT_DIALOG_DATA) public historicoApuesta: HistoricoApuesta ) { }

  ngOnInit(): void {
    // console.log(this.historicoApuesta);
    this.apuestas = this.historicoApuesta.apuestas;
  }

}
