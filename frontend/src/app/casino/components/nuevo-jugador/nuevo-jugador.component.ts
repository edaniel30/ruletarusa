import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { Jugador } from '../../interfaces/jugadores.interface';
import { CasinoService } from '../../services/casino.service';

@Component({
  selector: 'app-nuevo-jugador',
  templateUrl: './nuevo-jugador.component.html',
  styles: [
  ]
})
export class NuevoJugadorComponent implements OnInit {

  constructor(  private fb: FormBuilder,
                private casinoService: CasinoService,
                private dialogRef: MatDialogRef<NuevoJugadorComponent>,
                @Inject(MAT_DIALOG_DATA) public jugador: Jugador) { }

  formulario: FormGroup = this.fb.group({
    nombre:         [  , [Validators.required] ],
    dinero:         [  , [Validators.required] ],
    identificacion: [],

  });

  nombreJugador = '';

  ngOnInit(): void {

    // setea la info del formulario con la proveniente del padre
    this.formulario.reset( this.jugador );
    if ( this.jugador._id === '' ){
      this.formulario.controls.nombre.setValue('');
    }
  }

  crearJugador(): void {
    this.casinoService.crearJugador( this.formulario.value )
      .subscribe( resp => {
        // console.log(resp);
        if ( resp.ok ){
          Swal.fire('¡Completado!', resp.msg, 'success');
          this.dialogRef.close();
        } else {
          Swal.fire('¡Oops... algo salió mal!', resp.error.errors[0].msg, 'error');
        }
      });
  }

  editarJugador(): void {

    this.casinoService.editarJugador( this.jugador._id, this.formulario.value )
      .subscribe( resp => {
        // console.log(resp);
        if ( resp.ok ){
          Swal.fire('¡Completado!', resp.msg, 'success');
          this.dialogRef.close();
        } else {
          Swal.fire('¡Oops... algo salió mal!', resp.error.errors[0].msg, 'error');
        }
      });
  }

}
