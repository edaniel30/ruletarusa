const { response } = require("express");
const { randomNumber } = require("../helpers/calculos");

// models
const Player = require("../models/player");
const Apuesta = require("../models/apuesta");

// ******************************************************
const crearJuego = async ( req, res = response) => {

    try {

        const rojos = [1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36];
        const negros = [2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35];
     
        const numeroAzar = await randomNumber( 0, 37 );
        console.log('Numero en la ruleta:', numeroAzar);

        const { apuestas } = req.body;
        // console.log(apuestas);

        let ganadores = [];

        // Se revisa aca apuesta realizada 
        for ( i in apuestas ){

            apuestas[i].valorApuesta = apuestas[i].valorApuesta.toFixed(2); 

            let jugador = apuestas[i].jugador;
            let valorApuesta = apuestas[i].valorApuesta;
            let numeroApostado = apuestas[i].numero;

            // si el jugador gana, se crea el valor que gana el jugador dependiendo de su apuesta
            let ganancia = 0;
            if( numeroAzar === numeroApostado ){
                if ( rojos.includes( numeroAzar ) || negros.includes( numeroAzar ) ){ 
                    ganancia = valorApuesta * 2 
                }
                if( numeroAzar === 0 ){
                    ganancia = valorApuesta * 10;
                }

                if ( !ganadores.includes( jugador.nombre ) ){
                    ganadores.push( jugador.nombre );
                }
            }
            
            // si el jugador no gana, ganancia = 0 => ganancia = - valor apostado para disminuir dinero del jugador
            if ( ganancia === 0 ){ ganancia = valorApuesta * -1 }

            // console.log('ganancia:', ganancia);
            // Actualizamos valor del dinero del jugador
            const judadorActualizado = await Player.findByIdAndUpdate( jugador._id, {'$inc': { dinero: ganancia }} );   
        }

        const nuevaApuesta = new Apuesta( { ganadores, apuestas, numero_ganador: numeroAzar} );
        await nuevaApuesta.save();

        return res.status(200).json({
            ok: true,
            msg: 'Juego terminado con exito',
            nuevaApuesta
        });

        
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error al crear juego'
        });
    }

}
// ******************************************************
const getApuestas = async ( req, res = response ) => {

    try {

        const historicoApuestas = await Apuesta.find();

        return res.status(200).json({
            ok: true,
            msg: 'Historial de apuestas enviado',
            historicoApuestas
        });

        
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error al obtener jugadores'
        });
    }
}

module.exports = {
    crearJuego,
    getApuestas,
}