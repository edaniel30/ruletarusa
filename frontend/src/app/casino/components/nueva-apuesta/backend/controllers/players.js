const { response } = require("express");

// models
const Player = require("../models/player");


// ******************************************************
const postPlayer = async ( req, res = response ) => {
    
    try {

        const player = req.body;
        console.log(player);

        const newPlayer = new Player( player );

        await newPlayer.save();

        return res.status(200).json({
            ok: true,
            msg: 'Jugador creado con exito',
            newPlayer
        });

        
    } catch (error) {      
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error al registrar jugador'
        });
    }
}
// ******************************************************
const getPlayers = async ( req, res = response ) => {

    try {

        // se traen los jugadores con estado true
        const jugadores = await Player.find({estado: true});

        return res.status(200).json({
            ok: true,
            msg: 'Lista de jugadores enviada',
            jugadores
        });

        
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error al obtener jugadores'
        });
    }
}
// ******************************************************
const editPlayer = async ( req, res = response ) => {

    try {

        // se desestructura el id desde los parametros
        const { id } = req.params;
        // console.log(req.params);
        const jugador = req.body;

        const judadorActualizado = await Player.findByIdAndUpdate( id, jugador );

        return res.status(200).json({
            ok: true,
            msg: 'Jugador editado correctamente',
            judadorActualizado
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error al editar el jugador'
        });
    }
}
// ******************************************************
const removePlayer = async ( req, res = response ) => {

    try {

        const { id } = req.params;
        const jugadorEliminado = await Player.findByIdAndUpdate( id, {estado:false} );
        
        return res.status(200).json({
            ok: true,
            msg: 'Jugador eliminado correctamente',
            jugadorEliminado
        });



    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error al eliminar el jugador'
        });
    }

}

module.exports = {
    postPlayer,
    getPlayers,
    editPlayer,
    removePlayer

}