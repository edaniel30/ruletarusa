const { Router } = require('express');
const { check } = require('express-validator');
const { postPlayer, getPlayers, editPlayer, removePlayer } = require('../controllers/players');
const { existPlayerIdentification, existPlayerId } = require('../helpers/de-validators');
const { validarCampos } = require('../middlewares/validar-campos');


const router = Router();

router.post('', [
    check('nombre', 'El nombre deljugador es obligatorio').not().isEmpty(),
    check('identificacion', 'El numero de identidad del jugador es obligatorio').not().isEmpty(),
    check('identificacion').custom( existPlayerIdentification ),
    validarCampos
], postPlayer);

router.get('', getPlayers);

router.put('/:id', [
    check('id', 'El id de jugador no es valido').isMongoId(),
    check('id').custom( existPlayerId ),
    validarCampos
], editPlayer)

router.delete('/:id', [
    check('id', 'El id de jugador no es valido').isMongoId(),
    check('id').custom( existPlayerId ),
    validarCampos
], removePlayer)


module.exports = router;