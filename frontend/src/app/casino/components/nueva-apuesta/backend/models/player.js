const { Schema, model } = require("mongoose");

const PlayersSchema = Schema({
    nombre: {
        type: String,
        require: true,
    },
    identificacion: {
        type: String,
        require: true,
        unique: true
    },
    dinero: {
        type: Number,
        require: true,
        default: 15000
    },
    estado: {
        type: Boolean,
        default: true
    },
});

// ApuntamientosSchema.methods.toJSON = function(){
//     const { __v, password,...apuntamiento } = this.toObject();
//     return apuntamiento;
// }

module.exports = model( 'Player', PlayersSchema );