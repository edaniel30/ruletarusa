// importación
const express = require( 'express' );
const cors = require( 'cors' ); 
const { dbConnection } = require('../db/config');

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;

        this.paths = {
            jugadores: '/casino/jugadores',
            apuestas: '/casino/apuestas',
        }

        // Conectar a DB
        this.conectarDB();


        // Middlewares
        this.middlewares();

        // lectura de parametros
        this.app.use( express.json() ); 

        // Rutas de mi apicación
        this.routes();
    }

    async conectarDB() {
        await dbConnection();
    }

    middlewares() {

        // CORS
        this.app.use(cors());

        // Directorio publico
        this.app.use( express.static( 'public' ) );

    }

    routes() {
        this.app.use( this.paths.jugadores, require('../routers/players') );
        this.app.use( this.paths.apuestas, require('../routers/apuestas') );
    }

    lister() {
        this.app.listen(  this.port , () => {
            console.log('Servidor corriendo en el puerto',  this.port );
        });
    }
}

module.exports = Server;