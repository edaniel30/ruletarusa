import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CasinoService } from '../../services/casino.service';
import { Jugador } from '../../interfaces/jugadores.interface';
import Swal from 'sweetalert2';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-nueva-apuesta',
  templateUrl: './nueva-apuesta.component.html',
  styles: [
  ]
})
export class NuevaApuestaComponent implements OnInit {

  colores: string[] = ['Verde', 'Rojo', 'Negro'];
  jugador: any;
  dineroApostado = 0;
  apuestaPercent = 0;
  haveMoney = true;
  selectedColor = '';
  numeros: number[] = [];

  rojos = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
  negros = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];


  jugadores: Jugador[] = [];
  jugadoresIn: any[] = [];

  nuevaApuesta: FormGroup = this.fb.group({
    jugador: [ , [Validators.required]],
    color: [ , [Validators.required]],
    numero: [ , [Validators.required]],
    apuesta: [ , [Validators.required, this.validacionManual]],
    valorApuesta: [ , [Validators.required]],
  });

  apuestaTotal: FormGroup = this.fb.group({
    apuestas: this.fb.array( [
      // [ , Validators.required]
    ], Validators.required)
  });

  // tslint:disable-next-line: typedef
  get apuestasArr() {
    return this.apuestaTotal.get( 'apuestas' ) as any;
  }

  constructor(  private fb: FormBuilder,
                private dialogRef: MatDialogRef<NuevaApuestaComponent>,
                private casinoService: CasinoService ) { }

  ngOnInit(): void {

    // se obtiene lista de jugadores registrados
    this.obtenerJugadores();

    // suscripcion a cambios de jugador que setea como null apuesta y valor apuesta
    this.nuevaApuesta.controls.jugador.valueChanges
      .subscribe( newValue => {
        this.nuevaApuesta.controls.apuesta.setValue( null );
        this.nuevaApuesta.controls.valorApuesta.setValue( null );

        this.jugador = this.nuevaApuesta.controls.jugador.value;

        if (  this.jugador && this.jugador.dinero === 0 ){
          this.haveMoney = false;
        } else { this.haveMoney = true; }
      });

    this.nuevaApuesta.controls.color.valueChanges
        .subscribe( newValue => {
          console.log(newValue);
          this.selectedColor = newValue;

          if ( newValue === 'Verde' ){
            this.numeros = [0];
          }
          if ( newValue === 'Rojo' ){
            this.numeros = this.rojos;
          }
          if ( newValue === 'Negro' ){
            this.numeros = this.negros;
          }
        });

    // se suscribe a valor de la apuesta, cada vez que cambia varia el valor de la apuesta
    this.nuevaApuesta.controls.apuesta.valueChanges
      .subscribe( newValue => {

        this.jugador = this.nuevaApuesta.controls.jugador.value;
        // this.apuestaPercent = this.nuevaApuesta.controls.apuesta;

        // si el porcentaje de la apuesta es correcto, calcula valor de apuesta
        if ( newValue >= 11 && newValue <= 19 ){
          // calcula el valor si existe jugador
          if ( this.jugador ){
            if ( this.jugador.dinero > 1000 ){
              this.dineroApostado = this.jugador.dinero * (newValue / 100);
              this.nuevaApuesta.controls.valorApuesta.setValue( this.dineroApostado );
            } else {
              this.nuevaApuesta.controls.valorApuesta.setValue( this.jugador.dinero );
              this.nuevaApuesta.controls.apuesta.setValue( 100 );
            }
          }
        }
      });
  }

  obtenerJugadores(): void {
    this.casinoService.obtenerJugadores()
      .subscribe( jugadores => {
        this.jugadores = jugadores.jugadores;
      });
  }

  agregarApostador(): void {

    if (this.nuevaApuesta.invalid) { return; }

    this.apuestasArr.push( this.fb.control( this.nuevaApuesta.value, Validators.required ) );
    this.nuevaApuesta.reset();
  }

  eliminarApuesta( i: number ): void {
    this.apuestasArr.removeAt(i);
  }

  iniciarJuego(): void {
    // console.log( this.apuestaTotal.value );
    this.casinoService.IniciarApuesta( this.apuestaTotal.value )
      .subscribe( resp => {
        // console.log(resp);
        if ( resp.nuevaApuesta.ganadores.length === 0){
          Swal.fire('¡Juego terminado!', `${resp.msg}, no hay ganadores`, 'success');
        }else {
          Swal.fire('¡Juego terminado!', `${resp.msg}, ganadores: ${resp.nuevaApuesta.ganadores[0]}`, 'success');
        }
        this.dialogRef.close();
      });
  }

  // Validacion de formulario
  validacionManual( control: FormControl ): object | null {


    if ( control.value === 100 ){ return null; }
    if ( control.value >= 11 && control.value <= 19 ){ return null; }

    return {validacionManual: true};

  }
}
