import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { Jugador, Jugadores } from '../interfaces/jugadores.interface';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NuevaApuesta } from '../interfaces/nuevaApuesta.interface';
import { HistoricoApuestas } from '../interfaces/apuestas.interface';

@Injectable({
  providedIn: 'root'
})
export class CasinoService {

  private baseUrl: string = environment.baseUrl;

  constructor(  private http: HttpClient ) { }

  // METODOS PARA GESTIÓN DE JUGADORES
  // **********************************************************
  obtenerJugadores( ): Observable<Jugadores> {
    const url = `${this.baseUrl}/jugadores`;
    return this.http.get<Jugadores>( url );
  }

  crearJugador( jugador: Jugador ): Observable<any>{
    const url = `${this.baseUrl}/jugadores`;
    return this.http.post<Jugador>( url, jugador )
    .pipe(
      catchError( err => of(err) )
    );
  }
  editarJugador( id: string, jugador: Jugador ): Observable<any>{
    const url = `${this.baseUrl}/jugadores/${id}`;
    return this.http.put<any>( url, jugador )
    .pipe(
      catchError( err => of(err) )
    );
  }
  eliminarJugador( id: string ): Observable<any> {
    const url = `${this.baseUrl}/jugadores/${id}`;
    return this.http.delete<any>( url );
  }

  // METODOS PARA GESTIÓN DE APUESTAS
  // **********************************************************
  IniciarApuesta( apuestas: any ): Observable<NuevaApuesta>{
    const url = `${this.baseUrl}/apuestas`;
    return this.http.post<NuevaApuesta>( url, apuestas );
  }
  obtenerHistoricoApuestas( ): Observable<HistoricoApuestas> {
    const url = `${this.baseUrl}/apuestas`;
    return this.http.get<HistoricoApuestas>( url );
  }


}
