import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Jugador } from '../../interfaces/jugadores.interface';
import { CasinoService } from '../../services/casino.service';
import { NuevoJugadorComponent } from '../../components/nuevo-jugador/nuevo-jugador.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.component.html',
  styles: [
    `
    table {
      width: 100%;
    }
    `
  ]
})
export class JugadoresComponent implements OnInit, AfterViewInit  {

  // Paginacion de la tabla
  itemsPerPage = 20;
  pageSizeOptions: number[] = [20, 50, 100];

  // Definicion de columnas de tabla
  displayedColumns: string[] = ['nombre', 'identificacion', 'dinero', 'acciones' ];
  displayed: string[] = ['nombre', 'identificacion', 'dinero'];


  constructor(  private casinoService: CasinoService,
                public dialog: MatDialog, ) { }

  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  data = false;
  jugadorFalso = {
    _id: '',
    dinero: 15000,
    nombre: 'Nuevo jugador'
  };

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {

    this.obtenerJugadores();

  }

  obtenerJugadores(): void {
    this.casinoService.obtenerJugadores()
      .subscribe( jugadores => {
        // console.log(jugadores);
        this.dataSource.data = jugadores.jugadores;
        this.data = jugadores.ok;
      });
  }

  editar( jugador: Jugador ): void {
    const dialogRef = this.dialog.open( NuevoJugadorComponent, {
      width: '700px',
      data: jugador
    });
    dialogRef.afterClosed()
    .subscribe ( resp => {
      this.obtenerJugadores();
    });
  }

  eliminar( jugador: Jugador ): void {

    Swal.fire({
      title: '¿Desea eliminar este jugador?',
      text: '¡Esta accion no tiene vuelta atrás!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.casinoService.eliminarJugador( jugador._id )
          .subscribe( resp => {
            if ( resp.ok ){
              Swal.fire(
                '¡Eliminado!',
                `${resp.msg}`,
                'success'
              );
              this.obtenerJugadores();
            }
          });
      }
    });
  }

}
