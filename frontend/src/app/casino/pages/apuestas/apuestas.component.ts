import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NuevaApuestaComponent } from '../../components/nueva-apuesta/nueva-apuesta.component';
import { CasinoService } from '../../services/casino.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { VerApuestasComponent } from '../../components/ver-apuestas/ver-apuestas.component';

@Component({
  selector: 'app-apuestas',
  templateUrl: './apuestas.component.html',
  styles: [
    `
    table {
      width: 100%;
    }
    `
  ]
})
export class ApuestasComponent implements OnInit, AfterViewInit, OnDestroy {

  // Paginacion de la tabla
  itemsPerPage = 20;
  pageSizeOptions: number[] = [20, 50, 100];

  // Definicion de columnas de tabla
  displayedColumns: string[] = ['fecha', 'ganadores', 'numero_ganador', 'acciones' ];
  displayed: string[] = ['fecha', 'ganadores', 'numero_ganador', 'dinero'];

  apuesta: any[] = [];
  // variable de intervalo
  id: any;

  inGame = false;

  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  constructor(  public dialog: MatDialog,
                private casinoService: CasinoService ) { }

  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  data = false;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    // console.log('DESTRUYENDO');
    if (this.id) {
      clearInterval(this.id);
    }
  }

  ngOnInit(): void {

    this.obtenerapuestas();

    this.iniciarApuesta();

    this.id = setInterval(() => {

      if ( !this.inGame ){
        // console.log('inicia Juego automatico');
        this.iniciarApuesta();
      }
    }, 180000);

  }

  obtenerapuestas(): any {
    this.casinoService.obtenerHistoricoApuestas()
      .subscribe( historico => {
        // console.log( historico );
        this.dataSource.data = historico.historicoApuestas;
        this.data = historico.ok;
      });
  }

  iniciarApuesta(  ): any {

    this.inGame = true;

    const dialogRef = this.dialog.open( NuevaApuestaComponent, {
      width: '800px',
      height: '500px',
      data: 'jugador'
    });
    dialogRef.afterClosed()
      .subscribe ( (resp: any) => {
        // console.log(resp);
        this.obtenerapuestas();
        this.inGame = false;
    });
  }

  verApuestas( apuestas: any): any {
    const dialogRef = this.dialog.open( VerApuestasComponent, {
      width: '700px',
      data: apuestas
    });
    dialogRef.afterClosed()
    .subscribe ( resp => {
      this.obtenerapuestas();
    });
  }

}
