import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasinoRoutingModule } from './casino-routing.module';
import { ApuestasComponent } from './pages/apuestas/apuestas.component';
import { JugadoresComponent } from './pages/jugadores/jugadores.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MaterialModule } from '../material/material.module';
import { NuevoJugadorComponent } from './components/nuevo-jugador/nuevo-jugador.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NuevaApuestaComponent } from './components/nueva-apuesta/nueva-apuesta.component';
import { VerApuestasComponent } from './components/ver-apuestas/ver-apuestas.component';
import { ColumNamePipe } from './pipes/colum-name.pipe';

@NgModule({
  declarations: [
    ApuestasComponent,
    JugadoresComponent,
    NavBarComponent,
    NuevoJugadorComponent,
    NuevaApuestaComponent,
    VerApuestasComponent,
    ColumNamePipe
  ],
  imports: [
    CommonModule,
    CasinoRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule

  ]
})
export class CasinoModule { }
