import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { JugadoresComponent } from './pages/jugadores/jugadores.component';
import { ApuestasComponent } from './pages/apuestas/apuestas.component';

const routes: Routes = [
  {
    path: '',
    component: NavBarComponent,
    children: [
      {
        path: 'jugadores',
        component: JugadoresComponent
      },
      {
        path: 'apuestas',
        component: ApuestasComponent
      },
      {
        path: '**',
        redirectTo: 'apuestas'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CasinoRoutingModule { }
