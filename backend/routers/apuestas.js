const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');

const { crearJuego, getApuestas } = require("../controllers/apuestas");

const router = Router();

router.post('', [
    check('apuestas', 'Debe reqalizar al menos una apuesta').not().isEmpty(),
    validarCampos
], crearJuego);

router.get('', getApuestas);

module.exports = router;