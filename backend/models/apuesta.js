const { Schema, model } = require("mongoose");

const ApuestasSchema = Schema({
    fecha: {
        type: Date,
        require: true,
        default: new Date()
    },
    ganadores: [{
        type: String,
        require: true,
        default: ''
    }],
    apuestas: {
        type: Object,
        require: true,
    },
    numero_ganador: {
        type: Number,
        require: true,
    },

});

module.exports = model( 'Apuesta', ApuestasSchema );