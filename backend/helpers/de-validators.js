const Player = require("../models/player");


const existPlayerIdentification = async ( identificacion ) => {

    const identification = await Player.findOne({ identificacion });
        if ( identification ){
            throw new Error( `Ya existe un usuario registrado con el número de identificación: ${identificacion}` )
        }
}

const existPlayerId = async ( id ) => {

    const jugador = await Player.findById( id )
    // console.log(jugador);
        if ( !jugador ){ 
            throw new Error( `No existe jugador con id: ${id}` )
        }
}

module.exports = {
    existPlayerIdentification,
    existPlayerId,
}