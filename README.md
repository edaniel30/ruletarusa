## Mesa de casino
Esta aplicación es una simulación de un juego de ruleta rusa donde existe una cantidad dinámica de jugadores (apostadores), la simulación cuenta con un CRUD para gestionar los jugadores, y una pantalla donde se puede ver el historial de las apuestas realizadas.
Para iniciar una apuesta los jugadores registrados que deseen entrar en la partida deben realizar su apuesta escogiendo el número, el color y el porcentaje de dinero que desean apostar.

La previsualización de la aplicacion se puede hacer [aquí](https://ruleta-casino.herokuapp.com/mesa-casino/apuestas).

Nota: 
**Para el desarrollo de este proyecto se han usado servicios gratuitos los cuales en algun momento pueden dejar de funcionar**

## Herramientas
Para la implementación de este proyecto se hace uso de:
- Node.js
- Angular
- Mongo DB (Atlas)
- Heroku

## Implementación en sistema UNIX
